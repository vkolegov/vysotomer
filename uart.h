//
//  uart.h
//  firmware
//
//  Created by Владислав Колегов on 13/05/2019.
//

#include <avr/io.h>

#ifndef uart_h
#define uart_h

#ifdef F_CPU
	#define BAUD 19200L // Скорость обмена данными
	#define UBRRL_value (F_CPU/(BAUD*16))-1 //Согласно заданной скорости подсчитываем значение для регистра UBRR
#endif

void initUART();

void UART_send_byte(uint8_t byte_to_send);
void UART_transmit_data(uint8_t* data_start, int8_t data_count);
void UART_send_string(char* str);



#endif /* uart_h */