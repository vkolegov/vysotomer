 /* Name: main.c
 * Author: Nastya Zuevskaya
 */

#define F_CPU 8000000UL //8Mhz


#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "extra_math.h"
#include "HD44780.h"
#include "twi.h"
#include "bme_280.h"
#include "math.h"
#include "uart.h"

//==================================ВСПОМОГАТЕЛЬНЫЕ DEFINE===================================

// Перезагрузка реализована так: включаем watch_dog с  минимальное значением ожидания и заставляем систему ничего не делать какое-то время
#define soft_reset()        \
do                          \
{                           \
    wdt_enable(WDTO_15MS);  \
    for(;;)                 \
    {                       \
    }                       \
} while(0)

typedef int bool; 										// Булева переменная
#define FALSE 0											// Ложь
#define TRUE 1											// Правда

#define LED1 PB1										// Первый светодиод - PORT B пин 1
#define LED2 PB0										// Второй светодиод - PORT B пин 0
#define BTN1 PD3										// Первая кнопка - PORT D пин 3
#define BTN2 PD2										// Вторая кнопка - PORT D пин 2
#define BTN3 PC3										// Третия кнопка - PORT C пин 3

//==================================ПЕРЕМЕННЫЕ===================================

//------------------------------------BME280-------------------------------------
volatile uint8_t bme280_current_settings[3];			// Текущие настройки датчика BME280
volatile uint8_t bme280_current_reg; 					// Регистр, с которым работаем в данный момент
// Чтение
volatile uint8_t* bme280_data_recipient; 				// Адрес куда будут записываться данные
volatile uint8_t bme280_current_data_count;				// Текущее количество прочитанных байт
volatile uint8_t bme280_max_data_count; 				// Ожидаемое количество данных за эту передачу

// Запись
volatile uint8_t bme280_write_buffer; 					// Буфер, через который будем записывать настройки датчика
volatile uint8_t bme280_last_written_byte; 				// В случае успешной передачи байта устройству, он сохраняется здесь

char bme280_current_action; 							// Текущее действие. 'r' - чтение / 'w' - запись

// Данные
uint8_t raw_calib_data[CALIBDATABYTES]; 				// Сырые калибровочные данные [0-31 32x8]
volatile uint8_t raw_data[RAWDATABYTES]; 				// Сырые данные от датчика [0-7 8x8]

volatile struct bme280_calib_data calib_data;			// Собранные из сырых, готовые калибровочные данные
volatile struct bme280_data_uncomp uncomp_data;			// Собранные из сырых, но не скомпенсированные показания датчика
volatile struct bme280_data_comp data;					// Скомпенсированные. готовые показания датчика

//------------------------------------ОСНОВНЫЕ-----------------------------------
volatile char data_to_show = 'p'; 						// Какие данные отображать изначально на экране. 'p' - давление, 'h' - влажность
volatile char uart_current_menu;						// В каком меню UART мы находимся
volatile bool uart_nonstop_data = FALSE;				// Нужно ли слать данные бесконечно

const char uart_main_menu_message[] = "Main menu:\n'c' - config,\n'd' - data,\n'r' - reboot,\n'x' - Abort, main menu";
const char uart_config_menu_message[] = "Config menu:\n'h' - ctrl_hum,\n'm' - ctrl_meas,\n'c' - config,\n'x' - Main menu";
const char uart_data_menu_message[] = "Data menu:\n'c' - calibration data,\n'm' - current measurements,\n'n' - nonstop current measurements,\n'x' - Abort, main menu";

volatile uint32_t pressure_1; 							// Значение давления в первой точке
volatile uint32_t pressure_2;							// Значение давления во второй точке

uint32_t t_fine;										// Магическая переменная для уменьшения расчетов

//=====================================МЕТОДЫ===================================

//---------------------МЕТОДЫ ДЛЯ ВЗАИМОДЕЙСТВИЯ С BME280--------------------
// Проверка существования BME280
void bme280_check_presence();

// Сброс и настройка BME280
void initBME280(uint8_t ctrl_hum_options, uint8_t ctrl_meas_options, uint8_t config_options);

// Запись данные в регистр BME280
bool bme280_write_to_reg(uint8_t reg, uint8_t data);

// Чтение данных BME280
void bme280_read_from_reg(uint8_t start_reg, uint8_t* data_recipient, uint8_t max_data_count);


// Работа с полученными данными
void parse_raw_data();
int32_t compensateTemp(uint32_t adc_T);
uint32_t compensatePress(uint32_t adc_P);
uint32_t compensateHum(uint32_t adc_H);
void compensate_data();
uint32_t calculate(uint32_t p1, uint32_t p2, int32_t t); // Вычисление высоты между двумя точками


//==================================МЕТОДЫ ВЫВОДА НА ЭКРАН===================================
void print_with_param(uint8_t line, char* message_str, uint8_t param);
void print_temperature(int32_t temperature);
void print_pressure(uint32_t pressure);
void print_humidity(uint32_t humidity);

//==================================ОСНОВНЫЕ МЕТОДЫ===================================
// Инициализация
void init();

// Рутина на случай ошибки
void error_routine();

// Управление диодами
void setled(uint8_t id, uint8_t state);

// Обработка ввода через UART
void UART_process_input();

// Начало работы по TWI
void twi_begin();

void init() {

	// Запрещаем прерывания
	cli();

	//==================================СБРОС ВАЖНЫХ ПЕРЕМЕННЫХ===================================
	bme280_current_reg = 0x00;
	bme280_data_recipient = 0;

	bme280_current_action = 0;

	// Текущие настройки - настройки по-умолчанию
	bme280_current_settings[0] = BME280_CTRL_HUM_DATA;
	bme280_current_settings[1] = BME280_CTRL_MEAS_DATA;
	bme280_current_settings[2] = BME280_CONF_DATA;

	// Начальное меню - главное
	uart_current_menu = 'm';

	pressure_1 = 0;
	pressure_2 = 0;
    
    //==================================НАСТРОЙКА ПОРТОВ===================================
    // Настраиваем пины порта D на вывод
	// Для передачи данных дисплею. AVR:PORTD[4..7] <-> HD44780:DB[4..7]
    // Порты для кнопок PD3, PD2 определяются на вход
	DDRD = (0<<BTN2)|(0<<BTN1)|(1<<PD4)|(1<<PD5)|(1<<PD6)|(1<<PD7);

	// пины E, RW, RS дисплея привязываем к пинам PORTC[0..2]
    DDRC = (1<<E)|(1<<RW)|(1<<RS);

	// Порты подключенные к лампам настраиваем на выход
	DDRB = (1<<LED1)|(1<<LED2);

	PORTB = 0;
	PORTC = 0;
	PORTD = 0;
	// PORTD |= (1<<BTN1)|(1<<BTN2); // Включаем pull-up на ножках
    
    // Настраиваем таймер
    // TCCR0 |= (0<<CS02)|(0<<CS01)|(1<<CS00); // Предделителя нет (был 1024)
    // TIMSK |= (1<<TOIE0); // Разрешить прерывание по переполнению таймера 0

    // Настраиваем внешние прерывания (кнопочки)
    GICR |= (1<<INT1)|(1<<INT0); // прерывание 1 на ноге PD3, т.е. кнопка BTN1 и INT0 на ноге PD2 те BTN2
    // MCUCR &= (0xFC)|(1<<ISC01)|(0<<ISC00); // маска 0b11111100 и включаем что надо (срабатывание на растущем фронте)
    MCUCR = (1<<ISC01)|(0<<ISC00)|(1<<ISC11)|(0<<ISC10); // включаем что надо (срабатывание на падающем фронте для двух регистров)

    // Настраиваем частоту работу шины twi
	TWBR = FreqTWBR;
	
	// Иницилизируем дисплей в 4бит режиме, 2 строки
	HD44780Init();

	// Инициализируем UART 
	initUART();

}

void error_routine()
{
	while (1) {
		UART_process_input();
	}
}

int main(void)
{	
	init();
	
	// Проверяем, есть ли BME280 на линии
	// Если нет - программа дальше не пойдет
	bme280_check_presence();

	//==================================НАСТРОЙКА BME280===================================
	initBME280(BME280_CTRL_HUM_DATA, BME280_CTRL_MEAS_DATA, BME280_CONF_DATA);


	//==================================КАЛИБРОВОЧНЫЕ ДАННЫЕ===================================

	// Читаем первую часть калиб. данных
	bme280_read_from_reg(0x88, &raw_calib_data, CALIBDATA1MAX);

	// Читаем вторую часть калиб. данных
	bme280_read_from_reg(0xE1, &raw_calib_data[25], CALIBDATA2MAX);

	// Разбираем калибровочные данные
	calib_data = parse_calib_data(raw_calib_data);

	print_calib_params();

	//==================================ОСНОВНОЙ ЦИКЛ===================================

    for(;;){
    	_delay_ms(250);

    	// Читаем сырые данные
		bme280_read_from_reg(0xF7, &raw_data, RAWDATAMAX);

		// Разбираем сырые данные
		parse_raw_data();

		// Обрабатываем байт полученный через UART
		UART_process_input();

		// Компенсируем значения полученные с датчика
		compensate_data();

		// Чистим экран
    	lcd_clear_screen();
    	// Выводим температуру
    	print_temperature(data.temperature);
    	// Переходим на вторую строку
        lcd_set_cursor_pos(2,0);


        if ( (pressure_1 != 0) && (pressure_2 != 0) ) {
        	lcd_print_string("h_d:\0");
        	uint32_t a = calculate(pressure_1, pressure_2, data.temperature);
        	lcd_print_number_2(a);
        } else if (data_to_show == 'h') {
        	print_humidity(data.humidity);
        } else {
        	print_pressure(data.pressure);
        	// print_pressure(pressure_1);
        }
        // Разрешаем прерывания
    	sei();
	}

    return 0;   /* never reached */
}

void setled(uint8_t id, uint8_t state) {
	char led = (id==1)?LED1:LED2;

	switch (state) {
		case 1:
			PORTB |= (1<<led);
			break;
		case 0:
			PORTB &= ~(1<<led);
			break;
	}
}

//==================================РАБОТА С ДАННЫМИ===================================

// Функция собирает из сырых данных нескомпенсированные (с помощью формулы и калибровочных данных) данные
void parse_raw_data() {
    
    uint32_t data_xlsb;
    uint32_t data_lsb;
    uint32_t data_msb;
    
    // Собираем давление из кусков
    
    data_msb = (uint32_t) raw_data[0] << 12;
    data_lsb = (uint32_t) raw_data[1] << 4;
    data_xlsb = (uint32_t) raw_data[2] >> 4;
    
    uncomp_data.press = data_msb | data_lsb | data_xlsb;

	// Собираем температуру из кусков

	data_msb = (uint32_t)raw_data[3] << 12;
	data_lsb = (uint32_t)raw_data[4] << 4;
	data_xlsb = (uint32_t)raw_data[5] >> 4;

	uncomp_data.temp = data_msb | data_lsb | data_xlsb;
    
    // Собираем влажность из кусков
    
    data_msb = (uint32_t)raw_data[6] << 8;
    data_lsb = (uint32_t)raw_data[7];
    
    uncomp_data.hum = data_msb | data_lsb;
}

// Функция компенсирует с помощью формулы и калибровочных данных значения температуры, давления и влажности
void compensate_data() {
	data.temperature = compensateTemp(uncomp_data.temp);
    data.pressure = compensatePress(uncomp_data.press);
    data.humidity = compensateHum(uncomp_data.hum);
}

// Функция компенсирует с помощью формулы и калибровочных данных значение влажности
uint32_t compensateHum(uint32_t adc_H)
{
    int32_t var1;
    int32_t var2;
    int32_t var3;
    int32_t var4;
    int32_t var5;
    uint32_t humidity;
    uint32_t humidity_max = 102400;
    
    var1 = t_fine - ((int32_t)76800);
    var2 = (int32_t)(adc_H * 16384);
    var3 = (int32_t)(((int32_t)calib_data.dig_H4) * 1048576);
    var4 = ((int32_t)calib_data.dig_H5) * var1;
    var5 = (((var2 - var3) - var4) + (int32_t)16384) / 32768;
    var2 = (var1 * ((int32_t)calib_data.dig_H6)) / 1024;
    var3 = (var1 * ((int32_t)calib_data.dig_H3)) / 2048;
    var4 = ((var2 * (var3 + (int32_t)32768)) / 1024) + (int32_t)2097152;
    var2 = ((var4 * ((int32_t)calib_data.dig_H2)) + 8192) / 16384;
    var3 = var5 * var2;
    var4 = ((var3 / 32768) * (var3 / 32768)) / 128;
    var5 = var3 - ((var4 * ((int32_t)calib_data.dig_H1)) / 16);
    var5 = (var5 < 0 ? 0 : var5);
    var5 = (var5 > 419430400 ? 419430400 : var5);
    humidity = (uint32_t)(var5 / 4096);
    if (humidity > humidity_max)
        humidity = humidity_max;
    
    return humidity;
}

// Функция компенсирует с помощью формулы и калибровочных данных значение давления
uint32_t compensatePress(uint32_t adc_P) {
    int32_t var1;
    int32_t var2;
    int32_t var3;
    int32_t var4;
    uint32_t var5;
    uint32_t pressure;
    uint32_t pressure_min = 30000;
    uint32_t pressure_max = 110000;
    
    var1 = (((int32_t)t_fine) / 2) - (int32_t)64000;
    var2 = (((var1 / 4) * (var1 / 4)) / 2048) * ((int32_t)calib_data.dig_P6);
    var2 = var2 + ((var1 * ((int32_t)calib_data.dig_P5)) * 2);
    var2 = (var2 / 4) + (((int32_t)calib_data.dig_P4) * 65536);
    var3 = (calib_data.dig_P3 * (((var1 / 4) * (var1 / 4)) / 8192)) / 8;
    var4 = (((int32_t)calib_data.dig_P2) * var1) / 2;
    var1 = (var3 + var4) / 262144;
    var1 = (((32768 + var1)) * ((int32_t)calib_data.dig_P1)) / 32768;
    /* avoid exception caused by division by zero */
    if (var1) {
        var5 = (uint32_t)((uint32_t)1048576) - adc_P;
        pressure = ((uint32_t)(var5 - (uint32_t)(var2 / 4096))) * 3125;
        if (pressure < 0x80000000)
            pressure = (pressure << 1) / ((uint32_t)var1);
        else
            pressure = (pressure / (uint32_t)var1) * 2;
        var1 = (((int32_t)calib_data.dig_P9) * ((int32_t)(((pressure / 8) * (pressure / 8)) / 8192))) / 4096;
        var2 = (((int32_t)(pressure / 4)) * ((int32_t)calib_data.dig_P8)) / 8192;
        pressure = (uint32_t)((int32_t)pressure + ((var1 + var2 + calib_data.dig_P7) / 16));
        if (pressure < pressure_min)
            pressure = pressure_min;
        else if (pressure > pressure_max)
            pressure = pressure_max;
    } else {
        pressure = pressure_min;
    }
    return pressure;
}

// Функция компенсирует с помощью формулы и калибровочных данных значение температуры
int32_t compensateTemp(uint32_t adc_T)
{
	int32_t var1, var2, T;

	var1 = (int32_t)( ((adc_T / 8) - ((int32_t)calib_data.dig_T1) *2) );
	var1 = (var1 * ((int32_t)calib_data.dig_T2)) / 2048;

	var2 = (int32_t)((adc_T / 16) - ((int32_t)calib_data.dig_T1));
	var2 = (((var2 * var2) / 4096) * ((int32_t)calib_data.dig_T3)) / 16384;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) / 256;

	return T;
}

uint32_t calculate(uint32_t p1, uint32_t p2, int32_t t) {

	// uint16_t T = (uint16_t)((t+27315)/100); // В кельвинах

	// uint16_t lnP2 = (uint16_t)(log(p2)*10); //
	// uint16_t lnP1 = (uint16_t)(log(p1)*10);

	// return (uint16_t)((29*T*(lnP2-lnP1))/10); // В метрах

	// LATEST
	// uint16_t T = (uint16_t)((t+27315)/100); // В кельвинах

	// uint32_t lnP2 = (uint32_t)(log(p2)*10000); //
	// uint32_t lnP1 = (uint32_t)(log(p1)*10000);

	// return (uint16_t)((29*T*abs((lnP2-lnP1)))); // В метрах


	uint32_t T = (uint32_t)((t+27315)/100); // В кельвинах

	uint32_t lnP2 = (uint32_t)(log(p1)*100000); //
	uint32_t lnP1 = (uint32_t)(log(p2)*100000);
	// return lnP2;
	return (uint32_t)( (29*T*abs(lnP2-lnP1)) / 1000 ); // В сантиметрах
	// return lnP1;
}

//==================================ВЫВОД НА ЭКРАН===================================

// Функция выводит на экран на определенной линии (1, 2) сообщение message_str и следом выводит число param
void print_with_param(uint8_t line, char* message_str, uint8_t param) {
	lcd_set_cursor_pos(line, 0);
	lcd_print_string(message_str);
	lcd_print_number_2(param);
	_delay_ms(500);
}

// Функция выводит на экран все калибровочные параметры
void print_calib_params() {

	// lcd_clear_screen();

	// lcd_print_string("dig_T1: \0");
	// lcd_print_number_2(calib_data.dig_T1);
	// _delay_ms(700);
 //    lcd_clear_screen();
    
	// lcd_set_cursor_pos(1,0);
	// lcd_print_string("dig_T2: \0");
	// lcd_print_number_2(calib_data.dig_T2);
 //    _delay_ms(700);
 //    lcd_clear_screen();

	// lcd_set_cursor_pos(1,0);
	// lcd_print_string("dig_T3: \0");
	// lcd_print_number_2(calib_data.dig_T3);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P1: \0");
 //    lcd_print_number_2(calib_data.dig_P1);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P2: \0");
 //    lcd_print_number_2(calib_data.dig_P2);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_print_string("dig_P3: \0");
 //    lcd_print_number_2(calib_data.dig_P3);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P4: \0");
 //    lcd_print_number_2(calib_data.dig_P4);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P5: \0");
 //    lcd_print_number_2(calib_data.dig_P5);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P6: \0");
 //    lcd_print_number_2(calib_data.dig_P6);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P7: \0");
 //    lcd_print_number_2(calib_data.dig_P7);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_print_string("dig_P8: \0");
 //    lcd_print_number_2(calib_data.dig_P8);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_P9: \0");
 //    lcd_print_number_2(calib_data.dig_P9);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_H1: \0");
 //    lcd_print_number_2(calib_data.dig_H1);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_H2: \0");
 //    lcd_print_number_2(calib_data.dig_H2);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_H3: \0");
 //    lcd_print_number_2(calib_data.dig_H3);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_print_string("dig_H4: \0");
 //    lcd_print_number_2(calib_data.dig_H4); // 0x134 = 308 dec
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_H5: \0");
 //    lcd_print_number_2(calib_data.dig_H5); // 0x35 = 53 dec
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
 //    lcd_set_cursor_pos(1,0);
 //    lcd_print_string("dig_H6: \0");
 //    lcd_print_number_2(calib_data.dig_H6);
 //    _delay_ms(700);
 //    lcd_clear_screen();
    
    
}

// В цельсиях
void print_temperature(int32_t temperature) {
	lcd_print_string("t: \0");
	struct divmod10_t divmod = divmodu10(temperature);
    uint32_t temp_quot = divmod.quot;
    uint32_t temp_rem = divmod.rem;
    divmod = divmodu10(temp_quot);
	lcd_print_number_2(divmod.quot);
    lcd_send_data('.');
    lcd_send_data(divmod.rem+48);
    lcd_send_data(temp_rem+48);
	lcd_send_data(0xDF); // значок температуры
	lcd_send_data('C'); // Цельсии
}

// В гектопаскалях
void print_pressure(uint32_t pressure) {
    lcd_print_string("P: \0");
    struct divmod10_t divmod = divmodu10(pressure);
    uint32_t press_quot = divmod.quot;
    uint32_t press_rem = divmod.rem;
    divmod = divmodu10(press_quot);
    lcd_print_number_2(divmod.quot);
    lcd_send_data('.');
    lcd_send_data(divmod.rem+48);
    lcd_send_data(press_rem+48);
    lcd_print_string("hPa"); // гектопаскали
}
// В процентах
void print_humidity(uint32_t humidity) {
    lcd_print_string("H: \0");

    struct divmod10_t res = divmodu10(humidity); // Отсекаем 3 знак после запятой
    
    res = divmodu10(res.quot); // Делим предыдущий результат на 10
    uint8_t digit_2 = res.rem; // Остаток от деления есть вторая цифра после запятой
    
    res = divmodu10(res.quot); // Делим предыдущий результат на 10
    uint8_t digit_1 = res.rem; // Остаток от деления пред. результата на 10 есть первая цифра после запятой
    
    uint8_t hum_int_part = res.quot; // Результат деления - 2-3 цифры не больше 100. 8бит хватит
    
    lcd_print_number_2(hum_int_part);
    lcd_send_data('.');
    lcd_send_data(digit_1+48);
    lcd_send_data(digit_2+48);
    
    lcd_send_data('%'); // проценты
}

//==================================BME280===================================

void bme280_check_presence() {

	uint8_t bme280_read_id = 0;

	// Читаем ID
	bme280_read_from_reg(0xD0, &bme280_read_id, 1);

	// Считанный ID будет лежать в bme280_read_buffer
	if (bme280_read_id == BME280ID) {
		print_with_param(1, "BME280 OK: ", BME280ID);
		UART_send_string("BME280 OK");
	} else {
		print_with_param(1, "BME280 NOK: ", bme280_read_id);
		print_with_param(2, "EXPECTED: ", BME280ID);
		UART_send_string("BME280 NOT OK");
		UART_send_byte(bme280_read_id);
		
		error_routine();
	}

	_delay_ms(300); // Ждем пока все прочтут
}

void resetBME280() {

	// Регистр reset 0xE0, спец. значение - 0xB6
	bme280_write_to_reg(0xE0, 0xB6);

	if (bme280_last_written_byte == 0xB6) {
		UART_send_string("BME280 RESET");
	} else {
	 lcd_clear_screen();
		lcd_print_string("BME280 NOT RESET\0");
		print_with_param(2, "LWB(dec): ", bme280_last_written_byte);

		error_routine();
	}
}

// Сброс и конфигурирование BME280
void initBME280(uint8_t ctrl_hum_options, uint8_t ctrl_meas_options, uint8_t config_options) {

	bool status = FALSE;

	// Делаем мягкий сброс
	resetBME280();

	// Отправляем настройки измерения влажности
	// Ожидаем 1 (dec) 0x01
	status = bme280_write_to_reg(BME280_CTRL_HUM_REG, ctrl_hum_options);
	if (status) {
		UART_send_string("CTRL_HUM OK:");
		UART_send_byte(bme280_last_written_byte);
	} else {
		print_with_param(1, "CTRL_HUM NOK:", bme280_last_written_byte);
		print_with_param(2, "EXPECTED:", BME280_CTRL_HUM_DATA); // expecting 1 dec
		
		error_routine();
	}

	// Отправляем настройки измерения температуры и давления
	// Ожидаем 87 (dec) 0x57
	status = bme280_write_to_reg(BME280_CTRL_MEAS_REG, ctrl_meas_options);
	if (status) {
		UART_send_string("CTRL_MEAS OK:");
		UART_send_byte(bme280_last_written_byte);
	} else {
		print_with_param(1, "CTRL_MEAS NOK:", bme280_last_written_byte);
		print_with_param(2, "EXPECTED:", BME280_CTRL_MEAS_DATA); // expecting 87 dec
		
		error_routine();
	}

	// Отправляем настройки частоты, фильтра и интерфейса
	// Ожидаем 16 (dec) 0x10
	status = bme280_write_to_reg(BME280_CONF_REG, config_options);
	if (status) {
		UART_send_string("CONFIG OK:");
		UART_send_byte(bme280_last_written_byte);
	} else {
		print_with_param(1, "CONFIG NOK:", bme280_last_written_byte);
		print_with_param(2, "EXPECTED:", BME280_CONF_DATA); // expecting 16 dec
		
		error_routine();
	}
}


bool bme280_write_to_reg(uint8_t reg, uint8_t data) {
	bme280_current_action = 'w';
	bme280_current_reg = reg;
	bme280_write_buffer = data;

	twi_begin();

	return (bme280_last_written_byte == data);
}

void bme280_read_from_reg(uint8_t start_reg, uint8_t* data_recipient, uint8_t max_data_count) {
	bme280_current_action = 'r';
	bme280_current_reg = start_reg;
	bme280_data_recipient = data_recipient;
	bme280_max_data_count = max_data_count;

	bme280_current_data_count = 0;

	twi_begin();
}

void bme280_write_data(uint8_t data) {

	if (bme280_last_written_byte != data) {
		twi_send_byte(data);
		bme280_last_written_byte = data;	
	} else {
		TWI_STOP
	}

}


void bme280_read_data(uint8_t* data_start, uint8_t byte_count) {

	if (bme280_current_data_count < byte_count) { // Если кол-во полученных байт меньше чем нужное

		data_start[bme280_current_data_count] = TWDR; // считываем 1 байт

		bme280_current_data_count++; // увеличиваем кол-во полученных байт на 1

		TWI_ACK // Говорим "ок, приняли" и датчик отсылает следующий байт
	} else {
		TWI_NACK // На данном этапе получили что хотели и шлем NACK датчик(у)
	}
}

//================================ПРЕРЫВАНИЯ=============================

void twi_begin() {
	setled(1,1);
	sei();
	TWI_START
	_delay_ms(10);
	cli();
	setled(1,0);
}

ISR(TWI_vect) {
	
	uint8_t TWSR_status_code = TWSR&0b11111000;

	switch (TWSR_status_code) {
		case 0x00: // Illegal start/stop
			lcd_send_data('n');
			break;
		case 0x08: // Передан START - кидаем на шину адрес устройства для записи
			twi_send_byte(BME280_WRITE_ADDRESS);
			break;
		case 0x18: // Передана команда SLA+W, бит ACK получен
			twi_send_byte(bme280_current_reg); // Отправляем адрес регистра куда будем писать
			break;
		case 0x28: // Передан байт данных, бит ACK получен
			if (bme280_current_action == 'r') {
				// Если мы сейчас читаем
				// То шлем повторный старт

				TWI_START 

			} else if (bme280_current_action == 'w') {
				// А если пишем
				// Отправляем данные в установленный ранее командой bme280_set_reg() регистр
				
				bme280_write_data(bme280_write_buffer);

			}
			break;
		case 0x10: // Передан повторный START, кидаем на шину адрес устройства для чтения
			twi_send_byte(BME280_READ_ADDRESS);
			break;
		case 0x40: // Передана команда SLA+R, бит ACK получен
			TWI_ACK // Говорим что готовы принимать
			break;
		case 0x50: // Получен байт данных и отдан бит  ACK
			bme280_read_data(bme280_data_recipient, bme280_max_data_count); 
			break;
		case 0x58: // Получен байт данных и отдан бит NO ACK
			TWI_STOP // Останавливаем передачу
			setled(1,0);
			break;
	}
}


ISR(TIMER0_OVF_vect){
    // заглушка таймера
}

// Нажатие кнопки 2
ISR(INT0_vect) {
	if (pressure_1 == 0) {
		setled(2,0);
		// pressure_diff = 0;
		pressure_1 = data.pressure;
	} else if (pressure_2 == 0) {
		pressure_2 = data.pressure;
	} else {
		// pressure_diff = pressure_1 - pressure_2;
		pressure_1 = 0;
		pressure_2 = 0;
		setled(2,1);
	}

}

// Нажатие кнопки 1
ISR(INT1_vect) {
	data_to_show = (data_to_show=='p'?'h':'p');
	// if (test == 0) {
	// 	setled(2,1);
	// 	test = 1;
	// } else {
	// 	setled(2,0);
	// 	test = 0;
	// }
}


//================================UART==================================


// Основное меню. 'c' - меню настроек, 'd' - меню данных
void UART_process_main_menu(char uart_char) {
	switch (uart_char) {
		case 'c':
			UART_send_string(uart_config_menu_message);
			uart_current_menu = 'c';
			break;
		case 'd':
			UART_send_string(uart_data_menu_message);
			uart_current_menu = 'd';
			break;
		default:
			UART_send_string("Unknown command [MAIN]");
			UART_send_string(uart_main_menu_message);
			break;
	}
}

// Бесконечное (блокирующее) ожидание ввода
char UART_input_wait() {
	while ( !( UCSRA & (1<<RXC) ) ) {
	}

	return UDR;
}

void UART_change_config(char reg_sym) {

	UART_send_string("Enter config byte:");

	char config_byte = UART_input_wait();

	switch (reg_sym) {
		case 'h':
			// ctrl_hum
			bme280_current_settings[0] = config_byte;
			break;
		case 'm':
			// ctrl_meas
			bme280_current_settings[1] = config_byte;
			break;
		case 'c':
			// config
			bme280_current_settings[2] = config_byte;
			break;
	}
	setled(2,0);
	initBME280(bme280_current_settings[0], bme280_current_settings[1], bme280_current_settings[2]);
}

// Меню настроек
void UART_process_config_menu(char uart_char) {
	switch (uart_char) {
		case 'h':
		case 'm':
		case 'c':
			UART_change_config(uart_char);
			break;
		case 'd':
			// Настройки по умолчанию
			initBME280(BME280_CTRL_HUM_DATA, BME280_CTRL_MEAS_DATA, BME280_CONF_DATA);
			break;
		default:
			UART_send_string("Unknown command [CONFIG]");
			UART_send_string(uart_config_menu_message);
			break;
	}
}

// Меню данных. 'm' - текущие сырые измерения, 'c' - калибровочные данные
void UART_process_data_menu(char uart_char) {
	switch (uart_char) {
		case 'c':
			// calibration data - калибровочные данные
			UART_transmit_data(&calib_data, 33);
			break;
		case 'm':
			// measurements - измерения
			UART_transmit_data(&uncomp_data, 12);
			break;
		case 'n':
			// nonstop - переключаем режим отправки данных без остановки
			uart_nonstop_data = !uart_nonstop_data;
			break;
		default:
			UART_send_string("Unknown command [DATA]");
			UART_send_string(uart_data_menu_message);
			break;
	}
}


void UART_process_input() {
	char uart_temp_char = 0;

	if (uart_nonstop_data) {
		UART_transmit_data(&uncomp_data, 12);
	}

	// Если байт был принят и не прочитан
	if ( ( UCSRA & (1<<RXC) ) ) {
		// Оповещаем общественность что байт был принят с помощью включения диода
		setled(2,1);
		// Считываем че там у хохлов
		uart_temp_char = UDR;
		// Работу закончили - выключаем диод
		setled(2,0);

		if (uart_temp_char == 'x') {
			// В любом случае возвращаемся в главное меню, прекращаем передачу
			UART_send_string(uart_main_menu_message);
			uart_current_menu = 'm';
			uart_nonstop_data = FALSE;
			return;
		}

		if (uart_temp_char == 'r') {
			// В любом случае перезагружаем устройство
			UART_send_string("Rebooting");
			soft_reset();
			return;
		}


		switch (uart_current_menu) {
			case 'c':
				// Сейчас находимся в настройках
				UART_process_config_menu(uart_temp_char);
				break;
			case 'd':
				// Сейчас находимся в получении данных
				UART_process_data_menu(uart_temp_char);
				break;
			case 'm':
				UART_process_main_menu(uart_temp_char);
				break;
		}

	} 
}

