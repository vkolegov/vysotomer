#include "twi.h"

void twi_send_byte(unsigned char twi_byte) {
	TWDR = twi_byte;
	TWI_SEND;
}