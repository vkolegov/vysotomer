//
//  bme_280.h
//  firmware
//
//  Created by Владислав Колегов on 17/01/2019.
//

#ifndef bme_280_h
#define bme_280_h

#include <avr/io.h>
#include <avr/interrupt.h>

//============BME 280=================
/*
 ; адрес 0b111011x0 чтение
 ; адрес 0b111011x1 запись
 ; x = 0 - SDO<->GND
 ; x = 1 - SDO<->Vddio
 ; ИТОГОВЫЙ АДРЕС 0b1110110X 0x76
 ; то есть либо 0xEC для записи либо 0xED для чтения
 */
#define BME280_WRITE_ADDRESS 0xEC // 0x76 и 0 бит = 0 - запись
#define BME280_READ_ADDRESS 0xED // 0x76 и 0 бит = 1 - чтение
/*
 ; config 0xF5
 ; 7,6,5 - продолжительность неактивного состоянии в нормальном режиме. (t_sb)
 ; 4,3,2 - временная константа IIR фильтра (filter)
 ; 0 - включает SPI для трех линий связи
 */
#define BME280_CONF_REG 0xF5
#define BME280_CONF_DATA 0b00010000
/*
 ; ctrl_hum 0xF2
 ; 3,2,1 - оверсэмплинг данных о влажности 1, 2, 4, 8, 16
 ; ВАЖНО! Запись сюда возымеет эффект только после операции записи в ctrl_meas
 */
#define BME280_CTRL_HUM_REG 0xF2
#define BME280_CTRL_HUM_DATA 0b00000001 // биты 7-4 = 1, 1 оверсэмлинг влажности[2:0] = 001;
/*
 ; ctrl_meas 0xF4
 ; 7,6,5 - оверсэмплинг температуры (osrs_t)
 ; 4,3,2 - оверсэмплинг давнления (osrs_p)
 ; 1, 0 - режим сенсора (mode)
 */
#define BME280_CTRL_MEAS_REG 0xF4
#define BME280_CTRL_MEAS_DATA 0b01010111 // 010 оверсэмплинг температуры 2, 101 оверсэмплинг давления, 11 нормальный режим


#define BME280ID 0x60 //96 dec

// Соединяем младший и старший байты в одно 16бит число
#define BME280_CONCAT_BYTES(msb, lsb)     (((uint16_t)msb << 8) | (uint16_t)lsb)

#define CALIBDATABYTES 32
#define RAWDATABYTES 8

#define CALIBDATA1MAX 25
#define CALIBDATA2MAX 7
#define RAWDATAMAX 8


// Калибровочные данные (33 байта)
struct bme280_calib_data
{
    uint16_t dig_T1; // 2
    int16_t dig_T2; // 4
    int16_t dig_T3; // 6
    uint16_t dig_P1; // 8
    int16_t dig_P2; // 10 
    int16_t dig_P3; // 12
    int16_t dig_P4; // 14
    int16_t dig_P5; // 16
    int16_t dig_P6; // 18
    int16_t dig_P7; // 20
    int16_t dig_P8; // 22
    int16_t dig_P9; // 24
    uint8_t dig_H1; // 25
    int16_t dig_H2; // 27
    uint8_t dig_H3; // 28
    int16_t dig_H4; // 30
    int16_t dig_H5; // 32
    int8_t dig_H6; // 33
};

// Нескомпенсированнные данные с датчика (12 байт)
struct bme280_data_uncomp
{
    uint32_t press; // давление (4 байта)
    uint32_t temp; // температура (4 байта)
    uint32_t hum; // влажность (4 байта)
};

// Скомпенсированнные данные с датчика
struct bme280_data_comp
{
    int32_t temperature;
    uint32_t pressure;
    uint32_t humidity;
    
};


//bool bme280_set_ctrl_hum();
//bool bme280_set_ctrl_meas();
//bool bme280_set_config();

struct bme280_calib_data parse_calib_data(uint8_t* raw_calib_data);
#endif /* bme_280_h */
