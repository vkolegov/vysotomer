#ifndef extra_math_h
#define extra_math_h

#include <avr/io.h>

struct divmod10_t
{
    uint32_t quot;
    uint8_t rem;
};

struct divmod10_t divmodu10(uint32_t n);

#endif /* extra_math_h */
