#ifndef twi_h
#define twi_h

#include <avr/io.h>

#ifdef F_CPU
	#define FreqSCL 200000 // частота работы i2c 2 кГц
	#define FreqTWBR ((F_CPU/FreqSCL) - 16)/2 // частота предделителя. TWPS=0
#endif

// запускаем старт(TWSTA), TWINT, TWEN, TWIE - разрешаем интерфейс и прерывания
#define TWI_START (TWCR = (1<<TWSTA)|(1<<TWINT)|(1<<TWEN)|(1<<TWIE));
//стоп (TWSTO) и разрешаем интерфейс (TWEN). все остальное 0 - прерывания до нового старта не нужны
#define TWI_STOP (TWCR = (1<<TWSTO)|(1<<TWINT)|(1<<TWEN)|(0<<TWIE));
// Разрешаем интерфейс и прерывания. 
// При выставленном TWDR это значит отправить данные
#define TWI_SEND (TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE));
// Шлем ACK (TWEA=1)
#define TWI_ACK (TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN)|(1<<TWIE));
// Шлем NACK (TWEA=0)
#define TWI_NACK (TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE));


void twi_send_byte(unsigned char twi_byte);

#endif /* twi_h */
