#ifndef HD44780_h
#define HD44780_h

#include <avr/io.h>
#include <util/delay.h>
#include "extra_math.h"

#define E PC0
#define RW PC1
#define RS PC2

void HD44780Init(void);
void lcd_print_string(char* string);
// void lcd_print_number(int32_t number);
void lcd_print_number_2(int32_t value);
void lcd_set_cursor_pos(unsigned char line, unsigned char offset);
void lcd_clear_screen();
void lcd_send_command(unsigned char command);
void lcd_send_data(unsigned char data);
void lcd_send_byte(unsigned char byte_to_lcd);
void strob0(void);
void strob1(void);

#endif /* HD44780_h */
