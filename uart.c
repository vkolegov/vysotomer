//
//  uart.c
//  firmware
//
//  Created by Владислав Колегов on 13/05/2019.
//

#include "uart.h"

void initUART()
{
	UBRRL =(unsigned char) UBRRL_value;       //Младшие 8 бит UBRRL_value
	UBRRH = (unsigned char) (UBRRL_value >> 8);  //Старшие 8 бит UBRRL_value
	UCSRB |= (1<<TXEN) | (1<<RXEN);         //Бит разрешения передачи
	UCSRC |= (1<< URSEL)|(1<< UCSZ0)|(1<< UCSZ1); //Устанавливем формат 8 бит данных
}


void UART_send_byte(uint8_t byte_to_send) {

	// содержимое регистра UDR передано в сдвиговый регистр, т.е приёмопередатчик готов к передаче нового байта.
	// пока 5 бит (UDRE) в UCSRA не будет равен единице - цикл будет выполняться. то есть будем тупить
	while( !(UCSRA & (1<<UDRE) ) ) {
		// тупим
	}
	// Отсылаем байт
	UDR = byte_to_send;
}

void UART_transmit_data(uint8_t* data_start, int8_t data_count){
	int8_t i = 0;

	for(i = 0; i < data_count; i++)
	{
		UART_send_byte( *(data_start+i) );
	}
	
}

void UART_send_string(char* str) {

	uint8_t i = 0;

	for (i = 0; str[i] != '\0'; i++)
	{
		UART_send_byte(str[i]);
	}

	UART_send_byte('\n'); // В конце отправляем символ новой строки
}