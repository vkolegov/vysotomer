//
//  bme_280.c
//  firmware
//
//  Created by Владислав Колегов on 17/01/2019.
//

#include "bme_280.h"

struct bme280_calib_data parse_calib_data(uint8_t* raw_calib_data) {
    struct bme280_calib_data calib_data;
    // Temperature coeffs
    calib_data.dig_T1 = BME280_CONCAT_BYTES(raw_calib_data[1], raw_calib_data[0]);
    calib_data.dig_T2 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[3], raw_calib_data[2]);
    calib_data.dig_T3 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[5], raw_calib_data[4]);
    // Pressure coeffs
    calib_data.dig_P1 = BME280_CONCAT_BYTES(raw_calib_data[7], raw_calib_data[6]);
    calib_data.dig_P2 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[9], raw_calib_data[8]);
    calib_data.dig_P3 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[11], raw_calib_data[10]);
    calib_data.dig_P4 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[13], raw_calib_data[12]);
    calib_data.dig_P5 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[15], raw_calib_data[14]);
    calib_data.dig_P6 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[17], raw_calib_data[16]);
    calib_data.dig_P7 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[19], raw_calib_data[18]);
    calib_data.dig_P8 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[21], raw_calib_data[20]);
    calib_data.dig_P9 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[23], raw_calib_data[22]);
    // Humidity coeffs
    calib_data.dig_H1 = (uint8_t)raw_calib_data[24];
    calib_data.dig_H2 = (int16_t)BME280_CONCAT_BYTES(raw_calib_data[26], raw_calib_data[25]);
    calib_data.dig_H3 = (uint8_t)raw_calib_data[27];
    calib_data.dig_H4 = (int16_t) ( ((uint16_t)raw_calib_data[28] << 4 ) | (raw_calib_data[29]&0x0F) );
    calib_data.dig_H5 = (int16_t) ( ((uint16_t)raw_calib_data[30] << 4) | (raw_calib_data[29] >> 4) );
    calib_data.dig_H6 = (int8_t)raw_calib_data[31];
    return calib_data;
}
